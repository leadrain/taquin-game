class Grid {
  /**
   * Constructor
   * @param size grid size
   */
  constructor(size) {
    this.grid = createArray(5, 5).map(x => {
      x.fill(0);
      return x;
    });
    this.matrixSize = size;
    this.agents = [];
    this.agentPositions = [];
    this.agentsGoals = [];
  }

  /**
   * put the agents randomly in the grid and init their initial position and the expected position at the end of the game
   * @param agents list of agents
   */
  placeAgents(agents) {
    this.agents = agents;
    let filterResult;
    let columnIndexStart;
    let rowIndexStart;
    let columnIndexEnd;
    let rowIndexEnd;

    for (let i = 0; i < agents.length; i++) {
      do {
        rowIndexStart = this.getRandomInt(this.matrixSize);
        columnIndexStart = this.getRandomInt(this.matrixSize);
        filterResult = this.agentPositions.filter(agentPosition => {
          return (
            agentPosition.row === rowIndexStart &&
            agentPosition.column === columnIndexStart
          );
        });
      } while (filterResult.length > 0);
      this.agentPositions[agents[i].id] = {
        row: rowIndexStart,
        column: columnIndexStart
      };

      do {
        columnIndexEnd = this.getRandomInt(this.matrixSize);
        rowIndexEnd = this.getRandomInt(this.matrixSize);
        filterResult = this.agentsGoals.filter(agentGoal => {
          return (
            agentGoal.row === rowIndexEnd && agentGoal.column === columnIndexEnd
          );
        });
      } while (filterResult.length > 0);
      this.agentsGoals[agents[i].id] = {
        row: rowIndexEnd,
        column: columnIndexEnd
      };
    }
  }

  /**
   * @param state
   * @returns {boolean}
   */
  stateIsOccupied(state) {
    return (
      this.agentPositions.filter(agentPosition => {
        return (
          agentPosition.row === state.x && agentPosition.column === state.y
        );
      }).length > 0
    );
  }

  /**
   *
   * @param agent
   * @returns {row, column} the current position of the agent
   */
  getStateOf(agent) {
    return this.agentPositions[agent.id];
  }

  /**
   * @param agent
   * @returns {row, column} get the expected position for the agent
   */
  getTerminalStateOf(agent) {
    return this.agentsGoals[agent.id];
  }

  /**
   * @returns boolean if the user has won the game
   */
  isResolved() {
    for (let i = 0; i < this.agents.length; i++) {
      if (
        this.getStateOf(this.agents[i]).row !==
          this.getTerminalStateOf(this.agents[i]).row ||
        this.getStateOf(this.agents[i]).column !==
          this.getTerminalStateOf(this.agents[i]).column
      ) {
        return false;
      }
    }
    return true;
  }

  /**
   *
   * @param max
   * @returns number a index between 0 and max
   */
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  getGrid() {
    return this.grid;
  }

  /**
   *
   * @param position
   * @param row
   * @param column
   * @returns {{x: number, y: number}|undefined
   */
  getCoordinates(position, { row, column }) {
    switch (position) {
      case "NORTH":
        if (row - 1 < 0) return undefined;
        return { x: row - 1, y: column };
      case "WEST":
        if (column - 1 < 0) return undefined;
        return { x: row, y: column - 1 };
      case "EAST":
        if (column + 1 >= this.matrixSize) return undefined;
        return { x: row, y: column + 1 };
      case "SOUTH":
      default:
        if (row + 1 >= this.matrixSize) return undefined;
        return { x: row + 1, y: column };
    }
  }
}
