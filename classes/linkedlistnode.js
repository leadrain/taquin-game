class LinkedListNode {
    constructor(data, nextNode = null) {
        this.data = data;
        this.next = nextNode;
    }
}